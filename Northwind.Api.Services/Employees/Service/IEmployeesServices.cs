using System.Collections.Generic;
using Northwind.Api.Services.Employees.Models;

namespace Northwind.Api.Services.Employees.Service
{
    public interface IEmployeesServies
    {
        IEnumerable<Employee> GetAllEmployees();

        Employee GetById(int id);
    }
}