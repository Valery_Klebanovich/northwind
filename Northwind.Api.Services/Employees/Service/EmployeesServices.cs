﻿using System.Collections.Generic;
using System.Linq;

using Northwind.Api.Services.DbContext;
using Northwind.Api.Services.Employees.Mappers;
using Northwind.Api.Services.Employees.Models;

using DbEmployee = Northwind.Api.Services.DbContext.Employees;

namespace Northwind.Api.Services.Employees.Service
{
    public class EmployeesServies : IEmployeesServies
    {
        private readonly NorthwindDbContext _dbContext;

        public EmployeesServies(NorthwindDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public IEnumerable<Employee> GetAllEmployees()
        {
            IEnumerable<DbEmployee> dbEmployees = this._dbContext.Employees.ToList();
            var employees = dbEmployees.Select(EmployeeMapper.ConvertToModel).ToList();

            return employees;
        }

        public Employee GetById(int id)
        {
            DbEmployee dbEmployee = this._dbContext.Employees.SingleOrDefault(e => e.EmployeeID == id);

            return dbEmployee != null ? EmployeeMapper.ConvertToModel(dbEmployee) : null;
        }
    }
}