﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Northwind.Api.Services.DbContext;
using Northwind.Api.Services.Employees.Models;

using DbEmployee = Northwind.Api.Services.DbContext.Employees;

namespace Northwind.Api.Services.Employees.Mappers
{
    public static class EmployeeMapper
    {
        public static DbEmployee ConvertToDbModel(Employee employee)
        {
            return new DbEmployee()
            {
                EmployeeID = employee.EmployeeID,
                LastName = employee.LastName,
                FirstName = employee.FirstName,
                Title = employee.Title,
                TitleOfCourtesy = employee.TitleOfCourtesy,
                BirthDate = employee.BirthDate,
                HireDate = employee.HireDate,
                Address = employee.Address,
                City = employee.City,
                Region = employee.Region,
                PostalCode = employee.PostalCode,
                Country = employee.Country,
                HomePhone = employee.HomePhone,
                Extension = employee.Extension,
                Photo = employee.Photo,
                Notes = employee.Notes,
                ReportsTo = employee.ReportsTo,
                PhotoPath = employee.PhotoPath
            };
        }

        public static Employee ConvertToModel(DbEmployee dbEmployee)
        {
            return new Employee()
            {
                EmployeeID = dbEmployee.EmployeeID,
                LastName = dbEmployee.LastName,
                FirstName = dbEmployee.FirstName,
                Title = dbEmployee.Title,
                TitleOfCourtesy = dbEmployee.TitleOfCourtesy,
                BirthDate = dbEmployee.BirthDate,
                HireDate = dbEmployee.HireDate,
                Address = dbEmployee.Address,
                City = dbEmployee.City,
                Region = dbEmployee.Region,
                PostalCode = dbEmployee.PostalCode,
                Country = dbEmployee.Country,
                HomePhone = dbEmployee.HomePhone,
                Extension = dbEmployee.Extension,
                Photo = dbEmployee.Photo,
                Notes = dbEmployee.Notes,
                ReportsTo = dbEmployee.ReportsTo,
                PhotoPath = dbEmployee.PhotoPath
            };
        }
    }
}
