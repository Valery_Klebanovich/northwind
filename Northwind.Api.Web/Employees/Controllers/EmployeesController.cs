﻿using System.Collections.Generic;
using System.Web.Http;
using Northwind.Api.Services.DbContext;
using Northwind.Api.Services.Employees.Models;
using Northwind.Api.Services.Employees.Service;

namespace Northwind.WebApi.Employees.Controllers
{
    public class EmployeesController : ApiController
    {
        private readonly IEmployeesServies employeesServies = new EmployeesServies(new NorthwindDbContext());

        // GET: api/Employees
        public IEnumerable<Employee> Get()
        {
            IEnumerable<Employee> employees = this.employeesServies.GetAllEmployees();

            return employees;
        }

        // GET: api/Employees/5
        public Employee Get(int id)
        {
            Employee employee = this.employeesServies.GetById(id);

            return employee;
        }

        // POST: api/Employees
        public void Post([FromBody]Employee value)
        {
        }

        // PUT: api/Employees/5
        public void Put(int id, [FromBody]Employee value)
        {
        }

        // DELETE: api/Employees/5
        public void Delete(int id)
        {
        }
    }
}
