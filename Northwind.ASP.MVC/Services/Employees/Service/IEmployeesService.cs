﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Northwind.ASP.MVC.Services.Employees.Models;

namespace Northwind.ASP.MVC.Services.Employees.Service
{
    public interface IEmployeesService
    {
        IEnumerable<Employee> GetAllEmployees();

        Employee GetById(int id);
    }
}
