﻿using System;
using System.Collections.Generic;
using Northwind.ASP.MVC.Services.Employees.Models;
using Northwind.Platform.Web;

namespace Northwind.ASP.MVC.Services.Employees.Service
{
    public class EmployeesService: IEmployeesService
    {
        private readonly string employeesEndpoint = @"http://localhost:8081/api/Employees";

        private readonly HttpRequestor requestor = new HttpRequestor();

        public IEnumerable<Employee> GetAllEmployees()
        {
            IEnumerable<Employee> employees = this.requestor.MakeCall<IEnumerable<Employee>, string>(HttpMethod.GET, null, this.employeesEndpoint);
            
            return employees;
        }

        public Employee GetById(int id)
        {
            string url = string.Format(@"{0}/{1}", this.employeesEndpoint, id);

            Employee employee = this.requestor.MakeCall<Employee, string>(HttpMethod.GET, null, url);

            return employee;
        }
    }
}