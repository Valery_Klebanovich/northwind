﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using NLog;

namespace Northwind.ASP.MVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private readonly ILogger _logger = DependencyResolver.Current.GetService<ILogger>();

        protected void Application_Start()
        {
            
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            this._logger.Info("Application is started.");
        }

        protected void Application_End()
        {
            this._logger.Info("Application is stopped.");
        }
    }
}
