﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Northwind.ASP.MVC.Services.Employees.Models;

namespace Northwind.ASP.MVC.ViewModels.Builders
{
    public class EmployeeViewModelBuilder
    {
        public EmployeeViewModel Build(Employee employee)
        {
            byte[] image = null;
            using (var ms = new MemoryStream())
            {
                ms.Write(employee.Photo, 78, employee.Photo.Length - 78);
                //image = Image.FromStream(ms);
                image = ms.ToArray();
            }

            using (var ms = new MemoryStream(employee.Photo))
            {
                return new EmployeeViewModel()
                {
                    EmployeeId = employee.EmployeeID,
                    LastName = employee.LastName,
                    FirstName = employee.FirstName,
                    Title = employee.Title,
                    TitleOfCourtesy = employee.TitleOfCourtesy,
                    Address = employee.Address,
                    City = employee.City,
                    Country = employee.Country,
                    Notes = employee.Notes,
                    Photo = image
                };
            }
        }

        public IEnumerable<EmployeeViewModel> BuildMany(IEnumerable<Employee> employees)
        {
            return employees.Select(this.Build).ToList();
        }
    }
}