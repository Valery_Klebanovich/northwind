﻿using System.Web.Mvc;
using NLog;
using Northwind.ASP.MVC.Services.Employees.Service;
using Northwind.ASP.MVC.ViewModels.Builders;

namespace Northwind.ASP.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger _logger;
        private readonly IEmployeesService _employeesServies;

        public HomeController(ILogger logger, IEmployeesService employeesServies)
        {
            this._logger = logger;
            this._employeesServies = employeesServies;
        }

        public ActionResult Index()
        {
            this._logger.Info("Home/Index is opened.");

            var builder = new EmployeeViewModelBuilder();

            var employees = this._employeesServies.GetAllEmployees();
            var models = builder.BuildMany(employees);

            return View(models);
        }
        
        public ActionResult About()
        {
            this._logger.Info("Home/About is opened.");

            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            //Error.ArgumentNull()

            this._logger.Info("Home/Contact is opened.");

            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}