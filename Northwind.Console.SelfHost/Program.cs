﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;

namespace Northwind.Console
{
    class Program
    {
        private static async Task<int> GetNumberAsync(int minValue, int maxValue)
        {
            var number = new Random().Next(minValue, maxValue);

            System.Console.WriteLine($"GetNumberAsync: value={number}");
            
            Thread.Sleep(6000);

            return number;
        }

        private static async void Calculation()
        {
            System.Console.WriteLine("Calculation: method is started...");

            var result = await GetNumberAsync(0, 10);

            System.Console.WriteLine("Calculation: calculation...");

            var square = result;
            square *= square;

            System.Console.WriteLine($"Calculation: value={square}");
        }


        static void Main(string[] args)
        {
            //Calculation();

            using (WebApp.Start<Startup>("http://localhost:8000"))
            {
                System.Console.WriteLine("Сервер запущен. Нажмите любую клавишу для завершения работы...");
                System.Console.ReadLine();
            }

            System.Console.ReadKey();
        }
    }
}
