﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Northwind.Console.Controllers
{
    public class NumbersController: ApiController
    {
        public IEnumerable<int> Get()
        {
            return Enumerable.Range(0, 10);
        }

        [ActionName("Range")]
        public IEnumerable<int> GetRange(int min, int count)
        {
            return Enumerable.Range(min, count);
        }
    }
}
