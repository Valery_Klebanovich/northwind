﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Northwind.ASP.MVC.UnitTests
{
    [TestClass]
    public class CustomBindingTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            //IDictionary<Type, CustomBindAttribute[]> attributesByTypes = this.GetTypesByAttribute<CustomBindAttribute>();


        }

        //private void ValidateAttributeInitialization(IDictionary<Type, CustomBindAttribute[]> attributesByTypes)
        //{
            
        //}

        private IDictionary<Type, T[]> GetTypesByAttribute<T>() where T : class
        {
            Dictionary<Type, T[]> typeAttributesDictionary = new Dictionary<Type, T[]>();

            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly a in assemblies)
            {
                foreach (Type t in a.GetTypes())
                {
                    T[] typeAttributes = t.GetCustomAttributes(typeof (T), true) as T[];
                    if (typeAttributes.Length > 0)
                    {
                        typeAttributesDictionary.Add(t, typeAttributes);
                    }
                }
            }

            return typeAttributesDictionary;
        }
    }
}
