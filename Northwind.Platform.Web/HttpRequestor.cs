﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Northwind.Platform.Validators;

namespace Northwind.Platform.Web
{
    public enum HttpMethod
    {
        POST, 
        GET
    }

    public class HttpRequestor
    {
        public TResponse MakeCall<TResponse, TRequest>(HttpMethod method, string url, TRequest request, IReadOnlyDictionary<string, string> parameters = null) where TResponse : class where TRequest : class
        {
            Guard.ArgumentNotNullOrEmpty(url, "url");

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                string urlParameters = string.Empty;
                if (parameters != null && parameters.Count > 0)
                {
                    urlParameters = string.Join("&",
                        parameters.Select(kvp => string.Format("{0}={1}", kvp.Key, kvp.Value)));
                }

                HttpResponseMessage httpResponse = null;
                if (method == HttpMethod.GET)
                {
                    httpResponse = this.Get(client, urlParameters);
                }
                else
                {
                    string content = JsonConvert.SerializeObject(request);
                    httpResponse = this.Post(client, content, urlParameters); // Blocking call!
                }

                if (httpResponse == null || !httpResponse.IsSuccessStatusCode)
                {
                    Console.WriteLine("{0} ({1})", (int) httpResponse.StatusCode, httpResponse.ReasonPhrase);
                    throw new ApplicationException();
                }

                var responseBody = httpResponse.Content.ReadAsStringAsync().Result;
                TResponse response = JsonConvert.DeserializeObject<TResponse>(responseBody);

                return response;
            }
        }

        private HttpResponseMessage Get(HttpClient client, string urlParameters)
        {
            return client.GetAsync(urlParameters).Result;  // Blocking call!
        }

        private HttpResponseMessage Post(HttpClient client, string content, string urlParameters)
        {
            return client.PostAsync(urlParameters, new StringContent(content)).Result;  // Blocking call!
        }
    }
}
