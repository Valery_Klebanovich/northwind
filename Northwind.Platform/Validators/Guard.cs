﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Northwind.Platform.Validators
{
    public static class Guard
    {
        public static void ArgumentNotNull(object argumentValue, string argumentName)
        {
            if (argumentValue == null)
            {
                throw new ArgumentNullException(argumentName);
            }
        }

        public static void ArgumentNotNullOrEmpty(string argumentValue, string argumentName)
        {
            ArgumentNotNull(argumentValue, argumentName);

            if (string.IsNullOrEmpty(argumentValue))
            {
                throw new ArgumentException("The argument value cannot be null or empty string.", argumentName);
            }
        }

        public static void ArgumentNotNullOrEmpty<T>(IEnumerable<T> argumentValue, string argumentName)
        {
            ArgumentNotNull(argumentValue, argumentName);

            if (!argumentValue.Any())
            {
                throw new ArgumentException("The argument value cannot be an empty collection.", argumentName);
            }
        }

        public static void ArgumentIsGuid(string guid, string guidName)
        {
            ArgumentNotNull(guid, guidName);
            Guid guidToVerify;
            if (!Guid.TryParse(guid, out guidToVerify))
            {
                throw new ArgumentException("The guid value is an invalid guid: " + guid + ".", guidName);
            }
        }

        public static void ArgumentIsGuidAndNotEmpty(string guid, string guidName)
        {
            ArgumentNotNull(guid, guidName);
            Guid guidToVerify;
            if (!Guid.TryParse(guid, out guidToVerify))
            {
                throw new ArgumentException("The guid value is an invalid guid: " + guid + ".", guidName);
            }
            else if (guidToVerify == Guid.Empty)
            {
                throw new ArgumentException("The guid value cannot be an empty guid.", guidName);
            }
        }
    }
}
